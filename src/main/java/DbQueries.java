import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by dogor on 09.05.2017.
 */
public class DbQueries {
    public Connection dbConnection = null;
    public Statement statement = null;
    public ResultSet rs;

    DbQueries(){}

    public void insertIntoTables(String fname,String lname, String mark) throws SQLException {
        String insertFnameIntoTableSQL = "INSERT INTO first_name"
                + "(first_name) " + "VALUES"
                + "('"+fname+"')";

        String insertLnameIntoTableSQL = "INSERT INTO last_name"
                + "(last_name) " + "VALUES"
                + "('"+lname+"')";


        try {
            DbConnection connection = new DbConnection();
            dbConnection = connection.DbConnection();
            statement = dbConnection.createStatement();


            statement.execute(insertFnameIntoTableSQL);

            rs = statement.executeQuery("select last_insert_id()");
            rs.next();
            String lastid = rs.getString(1);

            String insertMarkIntoTableSQL = "INSERT INTO marks"
                    + "(fname_id, lname_id, mark) " + "VALUES"
                    + "('"+lastid+"',last_insert_id(),'"+mark+"')";

            statement.execute(insertLnameIntoTableSQL);
            statement.execute(insertMarkIntoTableSQL);


            System.out.println("Insert was finished.");
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            if (statement != null) {
                statement.close();
            }
            if (dbConnection != null) {
                dbConnection.close();
            }
        }
    }

    public void searchByFirstName(String fname) throws SQLException {
        //   SELECT mark,(SELECT last_name FROM last_name WHERE lname_id=marks.lname_id) as last_name FROM marks WHERE marks.fname_id IN (SELECT fname_id FROM first_name WHERE first_name = 'Vazgen');
        String searchFnameQuery = "SELECT mark, (SELECT last_name "
                + "FROM last_name WHERE lname_id=marks.lname_id) as last_name "
                + "FROM marks " + "WHERE "
                + "marks.fname_id IN (SELECT fname_id "
                + "FROM first_name WHERE first_name = '"+fname+"')";


        try {
            DbConnection connection = new DbConnection();
            dbConnection = connection.DbConnection();
            statement = dbConnection.createStatement();


            rs = statement.executeQuery(searchFnameQuery);
            while (rs.next()) {
                String mark = rs.getString("mark");
                String last_name = rs.getString("last_name");

                System.out.println("First Name : " + fname + "  Last Name: "
                        + last_name +"   Mark: "+mark);
            }


            System.out.println("Search was finished.");
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            if (statement != null) {
                statement.close();
            }
            if (dbConnection != null) {
                dbConnection.close();
            }
        }
    }

    public void deleteRowByName(String fname) throws SQLException {
        // getLnameId ->   SELECT lname_id FROM marks WHERE fname_id IN (SELECT fname_id FROM first_name WHERE first_name = 'Vazgen')
        // deleteRowFromMarks ->   SELECT mark,(SELECT last_name FROM last_name WHERE lname_id=marks.lname_id) as last_name FROM marks WHERE marks.fname_id IN (SELECT fname_id FROM first_name WHERE first_name = 'Vazgen');

        String getLnameIdFromMarksTable = "SELECT lname_id FROM marks "
                + "WHERE fname_id IN (SELECT fname_id "
                + "FROM first_name WHERE first_name = '" + fname + "')";
        String deleteRowFromMarksTable = "DELETE FROM marks WHERE fname_id "
                + "IN (SELECT fname_id FROM first_name WHERE first_name = '"+fname+"')";
        String deleteRowFromFirstNameTable = "DELETE FROM first_name "
                + "WHERE first_name = '" + fname + "'";



        try {
            DbConnection connection = new DbConnection();
            dbConnection = connection.DbConnection();
            statement = dbConnection.createStatement();

            ArrayList lastNameId = new ArrayList();
            rs = statement.executeQuery(getLnameIdFromMarksTable);
            while (rs.next()) {
                lastNameId.add(rs.getInt("lname_id"));
            }

            statement.execute(deleteRowFromMarksTable);
            statement.execute(deleteRowFromFirstNameTable);

            for (int i =0; i<lastNameId.size();i++){
                String deleteRowFromLastNameTable = "DELETE FROM last_name "
                        + "WHERE lname_id = " + lastNameId.get(i);
                statement.execute(deleteRowFromLastNameTable);
            }

            System.out.println("Delete was finished.");
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            if (statement != null) {
                statement.close();
            }
            if (dbConnection != null) {
                dbConnection.close();
            }
        }
    }

}

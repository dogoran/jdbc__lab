import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by dogor on 08.05.2017.
 */
public class DbConnection {

    Connection DbConnection() {
        Connection dbConnection = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            System.out.println("Where`s ur db driver? :C");
            System.out.println(e.getMessage());
            return dbConnection;
        }
        try {
            dbConnection = DriverManager.getConnection("jdbc:mysql://localhost:3306/db_marks?autoReconnect=true&useSSL=false", "root","root");
            return dbConnection;
        } catch (SQLException e) {
            System.out.println("Mb u forgot ur pswrd?");
            System.out.println(e.getMessage());
        }
        return dbConnection;
    }
}
